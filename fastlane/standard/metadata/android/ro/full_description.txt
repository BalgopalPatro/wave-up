WaveUp este o aplicație care vă <i>trezește telefonul</i> - pornește ecranul - atunci când faceți un <i>gest</i> deasupra senzorului de proximitate.

Am dezvoltat această aplicație pentru a evita să apăs butonul de pornire doar pentru a mă uita la ceas - ceea ce se întâmplă destul de des pe telefonul meu. Există deja alte aplicații care fac exact asta - și chiar mai multe. Am fost inspirat de Gravity Screen On/Off, care este o aplicație <b>grozavă</b>. Totuși, sunt un mare fan al software-ului cu sursă deschisă și încerc să instalez software liber (liber ca în libertate, nu doar gratis ca o bere gratis) pe telefonul meu dacă se poate. Cum nu am putut găsi o aplicație cu sursă deschisă care să facă asta, am creat-o eu. Dacă vă interesează, vă puteți uita la codul sursă:
https://gitlab.com/juanitobananas/wave-up

Doar faceți un gest deasupra senzorului de proximitate al telefonului dumneavoastră pentru a porni ecranul. Acesta este numit <i>modul gest</i> și poate fi dezactivat în ecranul de setări pentru a putea preveni pornirea accidentală a ecranului.

De asemenea va porni ecranul atunci când vă scoateți telefonul din buzunar sau poșetă. Acesta este numit <i>modul buzunar</i> și poate fi și el dezactivat din ecranul de setări.

Ambele moduri sunt activate în mod implicit.

Se poate bloca telefonul și închide ecranul dacă acoperiți senzorul de proximitate timp de o secundă (sau un alt timp specificat). Acest mod nu poartă un nume special dar poate fi oricum schimbat din ecranul de setări. Nu este activat în mod implicit.

Pentru persoanele care nu au auzit niciodată de senzorul de proximitate: este o chestie mică undeva în zona unde puneți urechea pe telefon atunci când vorbiți la el. Nu se poate vedea în mod normal dar este responsabil să transmită telefonului să oprească ecranul atunci când aveți o convorbire.

<b>Dezinstalează</b>

Această aplicație folosește permisiunea de Administrator dispozitiv. Astfel nu se poate dezinstala WaveUp în mod 'normal'.

Pentru a dezinstala, deschideți aplicația și folosiți butonul 'Dezinstalează WaveUp' din partea de jos a meniului.

<b>Probleme cunoscute</b>

Din păcate unele telefoane păstrează procesorul pornit atunci când monitorizează senzorul de proximitate. Aceasta se numește un <i>wake lock</i> și cauzează un consum ridicat al bateriei. Nu este vina mea că se întâmplă și nu pot face nimic pentru a schimba asta. Alte telefoane vor "adormi" atunci când ecranul este oprit dar încă vor monitoriza senzorul de proximitate. În acest caz, consumul bateriei este aproape zero.

<b>Permisii Android necesare:</b>

▸ WAKE_LOCK pentru a porni ecranul
▸ USES_POLICY_FORCE_LOCK pentru a bloca dispozitivul
▸ RECEIVE_BOOT_COMPLETED pentru rula automat la pornire, dacă este selectat
▸ READ_PHONE_STATE pentru a suspenda WaveUp în timpul convorbirilor telefonice

<b>Note diverse</b>

Aceasta este prima aplicație Android pe care am scris-o, așa că fiți atenți!

De asemenea este și prima mea contribuție în lumea open-source. În sfârșit!

Mi-ar face plăcere să îmi dați feedback de orice fel sau să contribuiți în orice mod!

Vă mulțumesc că ați citit!

Spiritul open-source este foarte tare!!!

<b>Traduceri</b>

Ar fi extraordinar dacă ați putea ajuta la traducerea WaveUp în limba dumneavoastră (poate chiar și versiunea în Engleză are nevoie de revizii). Proiectul este disponibil la tradus pe Transifex: https://www.transifex.com/juanitobananas/waveup/

<b>Mulțumiri</b>

În special pentru:

https://gitlab.com/juanitobananas/wave-up/#acknowledgments