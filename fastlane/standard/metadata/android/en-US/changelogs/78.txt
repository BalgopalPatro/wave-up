Dear Android 9 users, if you had hidden the notification, you might have to hide it again. Due to a big Android change (not allowing apps to read sensors in the background) WaveUp was forced to always show a notification. Hopefully, it'll still work if you hide it with the new setting.

New in 2.6.8
★ Fix uninstall button for Android 9.
★ Update Russian translation.

New in 2.6.7
★ Update Ukrainian translation.

New in 2.6.6 (a and b too)
★ Update Italian translation.
