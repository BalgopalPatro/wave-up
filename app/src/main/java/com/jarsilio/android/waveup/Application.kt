package com.jarsilio.android.waveup

import android.app.Application
import android.content.Context
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import com.squareup.leakcanary.LeakCanary
import org.acra.ACRA
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraMailSender
import org.acra.annotation.AcraNotification
import timber.log.Timber

const val MAX_TAG_LENGTH = 23

@AcraCore(buildConfigClass = BuildConfig::class)

@AcraMailSender(mailTo = "juam+waveup@posteo.net")

@AcraNotification(
        resTitle = R.string.acra_notification_title,
        resText = R.string.acra_notification_text,
        resChannelName = R.string.acra_notification_channel_name,
        resSendButtonText = R.string.acra_notification_send,
        resDiscardButtonText = android.R.string.cancel,
        resSendButtonIcon = R.drawable.email_icon_gray,
        resDiscardButtonIcon = R.drawable.cancel_icon_gray
)

class WaveUpApplication : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

        if (!com.jarsilio.android.waveup.BuildConfig.DEBUG) {
        // The following line triggers the initialization of ACRA
            ACRA.init(this)
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }
}
